'use strict';

let
    assert = require('assert'),
    Util = require('../src/Util');

describe('Util', () => {
    describe('#convertWordToBool()', () => {
        it('should throw exception if no parameter is sent', () => {
            assert.throws(Util.convertWordToBool, /^Error: String word is expected$/);
        });

        it('should return true if "Yes" is sent', () => {
            let
                word = 'Yes';

            let
                result = Util.convertWordToBool(word);

            assert.equal(result, true);
        });

        it('should throw exception if numeric type is sent', () => {
            let
                word = 3;

            assert.throws(() => {
                Util.convertWordToBool(word)
            }, /^Error: String word is expected$/);
        });

        it('should throw exception if non-valid string is sent', () => {
            let
                word = 'x';

            assert.throws(() => {
                Util.convertWordToBool(word)
            }, /^Error: x is not a valid boolean word$/);
        });
    });
});